package menv

import (
	"gitee.com/dennis-mxx/mxx-core-v2/mmeta"
	"strings"
)

type FileRender struct {
	Env  string
	Path string
}

func (ce *FileRender) SetConfigPath(path string) {
	ce.Path = path
}
func (ce *FileRender) GetFilePath() string {
	return ce.Path
}

func (ce *FileRender) SetEnv(env string) {
	ce.Env = env
}
func (ce *FileRender) GetEnv() string {
	return ce.Env
}
func (ce *FileRender) GetPath() string {
	return ce.Path
}

func (ce *FileRender) Render() string {
	configPath := ce.LoadFile()
	if mmeta.Get("logger.level") == "" {
		mmeta.SetSys("logger.level", "info")
	}
	if mmeta.Get("server_name") == "" {
		mmeta.SetSys("server_name", "KNOW SERVER NAME")
	}
	return configPath
}

func (ce *FileRender) LoadFile() string {
	if ce.Env == "" {
		panic("The current environmental parameters are not set")
	}

	configPath := ce.Path
	if configPath == "" {
		configPath = "./conf/config-$env.toml"
	}

	configPath = strings.Replace(configPath, "$env", ce.Env, 1)
	mmeta.ReadSysFile(configPath)
	return configPath
}
