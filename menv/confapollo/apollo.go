package confapollo

import (
	"fmt"
	"gitee.com/dennis-mxx/mxx-core-v2/mexception"
	"gitee.com/dennis-mxx/mxx-core-v2/mmeta"
	"github.com/apolloconfig/agollo/v4"
	"github.com/apolloconfig/agollo/v4/env/config"
	"github.com/apolloconfig/agollo/v4/storage"
	"strings"
)

var Client agollo.Client

func InitApollo() {
	appId := mmeta.Get("apollo.appId").(string)
	secret := mmeta.Get("apollo.secret").(string)
	ip := mmeta.Get("apollo.ip").(string)
	namespaceName := mmeta.Get("apollo.namespace_name").(string)
	isBackupConfig := mmeta.Get("apollo.isBackup_config").(bool)

	if appId == "" {
		panic("The configuration item [apollo.app_id] does not exist ")
	}
	if secret == "" {
		panic("The configuration item [apollo.secret] does not exist ")
	}
	if ip == "" {
		panic("The configuration item [apollo.ip] does not exist ")
	}
	if namespaceName == "" {
		panic("The configuration item [apollo.namespace_name] does not exist ")
	}

	c := &config.AppConfig{
		AppID:          appId,
		Cluster:        secret,
		IP:             ip,
		NamespaceName:  namespaceName,
		IsBackupConfig: isBackupConfig,
		Secret:         secret,
	}
	client, err := agollo.StartWithConfig(func() (*config.AppConfig, error) {
		return c, nil
	})
	if err != nil {
		panic(mexception.NewException(mexception.ERROR, fmt.Sprintf("初始化apollo配置失败 --> %s", err.Error())))
	}
	Client = client

	client.AddChangeListener(apolloChangeListener{})
	for _, item := range strings.Split(namespaceName, ",") {
		client.GetConfigCache(item).Range(func(key, value interface{}) bool {
			mmeta.SetSys(key.(string), value)
			return true
		})
	}
	mmeta.SetSys("server_name", appId)
}

type apolloChangeListener struct {
}

func (ce apolloChangeListener) OnChange(event *storage.ChangeEvent) {
	for k, v := range event.Changes {
		mmeta.SetSys(k, v)
	}
}
func (ce apolloChangeListener) OnNewestChange(event *storage.FullChangeEvent) {
	for k, v := range event.Changes {
		mmeta.SetSys(k, v)
	}
}
