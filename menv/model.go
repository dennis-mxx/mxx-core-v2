package menv

var Environment = &EnvironmentModel{}

type ConfigRender interface {
	SetConfigPath(path string)
	GetEnv() string
	GetPath() string
	SetEnv(env string)
	Render() string
}

type Apollo struct {
	AppId          string `toml:"app_id" json:"app_id"`
	Cluster        string `toml:"cluster" json:"cluster"`
	Ip             string `toml:"ip" json:"ip"`
	NamespaceName  string `toml:"namespace_name" json:"namespace_name"`
	IsBackupConfig bool   `toml:"is_backup_config" json:"is_backup_config"`
	Secret         string `toml:"secret" json:"secret"`
}

type EnvironmentModel struct {
	Value      string
	ServerName string   `toml:"server_name"`
	Banner     string   `toml:"banner"`
	Apollo     *Apollo  `toml:"apollo"`
	Mysql      *Mysql   `toml:"mysql"`
	Sqlite     *Sqlite3 `toml:"sqlite3"`
	Gin        *Gin     `toml:"gin"`
	Logger     *Logger  `toml:"logger"`
	Redis      *Redis   `toml:"redis"`
}
type Logger struct {
	Level string `toml:"level"`
	Dir   string `toml:"dir"`
}
type Redis struct {
	Network     string `toml:"network"`
	Addr        string `toml:"addr"`
	Password    string `toml:"password"`
	Db          int    `toml:"db"`
	ReadTimeout int    `toml:"read_timeout"`
	DialTimeout int    `toml:"dial_timeout"`
}

type Gin struct {
	Port          int64  `toml:"port"`    // 端口号
	Mode          string `toml:"release"` // 模式 debug,release,test
	Pprof         bool   `toml:"pprof"`
	PprofSecret   string `toml:"pprof_secret"`
	BasePath      string `toml:"base_path"` // 请求根路径
	Session       string `toml:"session"`
	SessionId     string `toml:"session_id"`
	SessionFile   string `toml:"session_file"`
	SessionSecret string `toml:"session_secret"`
}
type Mysql struct {
	Addr        string `toml:"username"`      // 用户名
	ShowSql     bool   `toml:"show_sql"`      // 打印sql
	MaxIdleConn int    `toml:"max_idle_conn"` // 最大闲置链接数
	MaxOpenConn int    `toml:"max_open_conn"` // 最大活跃连接数
	Timeout     int64  `toml:"timeout"`       // 请求超时时间（单位毫秒）
}

type Sqlite3 struct {
	Addr        string `toml:"addr"`          // 数据源地址
	ShowSql     bool   `toml:"show_sql"`      // 打印sql
	MaxIdleConn int    `toml:"max_idle_conn"` // 最大闲置链接数
	MaxOpenConn int    `toml:"max_open_conn"` // 最大活跃连接数
	Timeout     int64  `toml:"timeout"`       // 请求超时时间（单位毫秒）
}
