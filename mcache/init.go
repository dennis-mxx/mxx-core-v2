package mcache

import (
	"context"
	"gitee.com/dennis-mxx/mxx-core-v2/mbolt"
	"gitee.com/dennis-mxx/mxx-core-v2/mlogger"
	"github.com/allegro/bigcache/v3"
	"time"
)

var Default Cacheable

func InitCache(config bigcache.Config, ctx context.Context) Cacheable {
	cache, err := bigcache.New(ctx, config)
	if err != nil {
		mlogger.Out.Errorf("Failed to create BigCache instance:%v", err)
		panic(err)
	}
	return &MemoryCacheable{BigCache: cache}
}
func InitCache2(ctx context.Context) Cacheable {
	config := bigcache.Config{
		Shards:             1024,             // 分片数（必须为2的幂，选择相对较小的值）
		LifeWindow:         30 * time.Minute, // 条目过期时间，根据实际需求调整
		CleanWindow:        1 * time.Minute,  // 清理作业运行间隔，频繁清理以确保不会超过最大条目数
		MaxEntriesInWindow: 300000,           // 在 Life Window 内可以保存多少条目
		MaxEntrySize:       2048,             // 最大条目大小（字节），根据实际数据大小调整
		Verbose:            false,            // 禁用详细日志输出，以减少性能开销
		HardMaxCacheSize:   4096,             // 设置硬性最大缓存大小 (MB)，这里设置为4G。
	}

	return InitCache(config, ctx)
}
func InitCacheWithBlot(boltDB *mbolt.Bolt, bucket string, config bigcache.Config, ctx context.Context) Cacheable {
	cacheable := InitCache(config, ctx)
	return &BoltCacheable{Cache: cacheable, Bolt: boltDB, BucketName: bucket}

}
func InitCache2WithBlot(boltDB *mbolt.Bolt, bucket string, ctx context.Context) Cacheable {
	cacheable := InitCache2(ctx)
	return &BoltCacheable{Cache: cacheable, Bolt: boltDB, BucketName: bucket}

}

func InitDefault(config bigcache.Config, ctx context.Context) {
	Default = InitCache(config, ctx)
	GetDefault = getDefault
}

func InitDefault2(ctx context.Context) {
	Default = InitCache2(ctx)
	GetDefault = getDefault
}
func InitDefaultWithBlot(boltDB *mbolt.Bolt, bucket string, config bigcache.Config, ctx context.Context) {
	Default = InitCacheWithBlot(boltDB, bucket, config, ctx)
	GetDefault = getDefault
}

func InitDefault2WithBlot(boltDB *mbolt.Bolt, bucket string, ctx context.Context) {
	Default = InitCache2WithBlot(boltDB, bucket, ctx)
	GetDefault = getDefault
}
