package mcache

type Cacheable interface {
	Get(key string) (string, error)
	GetFunc(key string, fun func() []byte) (string, error)
	GetBytes(key string) ([]byte, error)
	GetBytesFunc(key string, fun func() []byte) ([]byte, error)
	Set(key string, value string) error
	SetBytes(key string, value []byte) error
	Delete(key string) error
	GetJsonFunc(key string, dest any, fun func() []byte) (any, error)
	GetJson(key string, dest any) (any, error)
	SetJson(key string, dest any) error
	Close() error
}
