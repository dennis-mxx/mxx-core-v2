package mcache

var GetDefault = func() Cacheable {
	if Default != nil {
		return Default
	}
	panic("big cache not initialize")
}

func Get(key string) (string, error) {
	return GetDefault().Get(key)
}

func GetFunc(key string, fun func() []byte) (string, error) {
	return GetDefault().GetFunc(key, fun)
}

func GetBytes(key string) ([]byte, error) {
	return GetDefault().GetBytes(key)
}
func GetBytesFunc(key string, fun func() []byte) ([]byte, error) {
	return GetDefault().GetBytesFunc(key, fun)
}
func Set(key string, value string) error {
	return GetDefault().Set(key, value)
}
func SetBytes(key string, value []byte) error {
	return GetDefault().SetBytes(key, value)
}
func Delete(key string) error {
	return GetDefault().Delete(key)
}
func GetJsonFunc(key string, dest any, fun func() []byte) (any, error) {
	return GetDefault().GetJsonFunc(key, dest, fun)
}
func GetJson(key string, dest any) (any, error) {
	return GetDefault().GetJson(key, dest)
}
func SetJson(key string, dest any) error {
	return GetDefault().SetJson(key, dest)
}
func Close() error {
	return GetDefault().Close()
}

func getDefault() Cacheable {
	return Default
}
