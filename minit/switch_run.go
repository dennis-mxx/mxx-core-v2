package minit

type SwitchRunner struct {
	Func map[string]func()
}

func (ce SwitchRunner) Start(funcName string) {
	fn := ce.Func[funcName]
	if fn != nil {
		fn()
	} else {
		println("runner func name not found >>>> ", funcName)
	}
}
