package mdb

type desc struct {
	fieldName string
	sortType  string
}

type Sorter struct {
	descList []desc
}

func Asc(fieldName ...string) *Sorter {
	return (&Sorter{}).Asc(fieldName...)
}
func Desc(fieldName ...string) *Sorter {
	return (&Sorter{}).Desc(fieldName...)
}

func (ew *Sorter) Desc(fieldName ...string) *Sorter {
	for i, _ := range fieldName {
		ew.descList = append(ew.descList, desc{fieldName: fieldName[i], sortType: "desc"})
	}
	return ew
}
func (ew *Sorter) Asc(fieldName ...string) *Sorter {
	for i, _ := range fieldName {
		ew.descList = append(ew.descList, desc{fieldName: fieldName[i], sortType: "asc"})
	}
	return ew
}
