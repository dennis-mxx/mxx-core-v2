package mdb

import (
	"xorm.io/xorm"
)

var Engine *xorm.Engine

type Entity interface {
	TableName() string
	IdName() string
}
