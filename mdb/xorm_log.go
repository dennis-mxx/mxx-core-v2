package mdb

import (
	"fmt"
	"gitee.com/dennis-mxx/mxx-core-v2/mlogger"
	"xorm.io/xorm/log"
)

type ZapXormLogger struct {
	level   log.LogLevel
	showSQL bool
}

// Debug empty implementation
func (ce *ZapXormLogger) Debug(v ...interface{}) {
	if ce.level <= log.LOG_DEBUG {
		mlogger.Out.Debug(v...)
	}
}

// Debugf empty implementation
func (ce *ZapXormLogger) Debugf(format string, v ...interface{}) {
	if ce.level <= log.LOG_DEBUG {
		mlogger.Out.Debug(fmt.Sprintf(format, v...))
	}
}

// Error empty implementation
func (ce *ZapXormLogger) Error(v ...interface{}) {
	if ce.level <= log.LOG_ERR {
		mlogger.Out.Error(v...)
	}
}

// Errorf empty implementation
func (ce *ZapXormLogger) Errorf(format string, v ...interface{}) {
	if ce.level <= log.LOG_ERR {
		mlogger.Out.Error(fmt.Sprintf(format, v...))
	}
}

// Info empty implementation
func (ce *ZapXormLogger) Info(v ...interface{}) {
	if ce.level <= log.LOG_INFO {
		mlogger.Out.Info(v...)
	}
}

// Infof empty implementation
func (ce *ZapXormLogger) Infof(format string, v ...interface{}) {
	if ce.level <= log.LOG_INFO {
		mlogger.Out.Info(fmt.Sprintf(format, v...))
	}
}

// Warn empty implementation
func (ce *ZapXormLogger) Warn(v ...interface{}) {
	if ce.level <= log.LOG_WARNING {
		mlogger.Out.Warn(v...)
	}
}

// Warnf empty implementation
func (ce *ZapXormLogger) Warnf(format string, v ...interface{}) {
	if ce.level <= log.LOG_WARNING {
		mlogger.Out.Warn(fmt.Sprintf(format, v...))
	}
}

// Level empty implementation
func (ce *ZapXormLogger) Level() log.LogLevel {
	return ce.level
}

// SetLevel empty implementation
func (ce *ZapXormLogger) SetLevel(l log.LogLevel) {
	ce.level = l
}

// ShowSQL empty implementation
func (ce *ZapXormLogger) ShowSQL(show ...bool) {
	if len(show) == 0 {
		ce.showSQL = true
		return
	}
	ce.showSQL = show[0]
}

// IsShowSQL empty implementation
func (ce *ZapXormLogger) IsShowSQL() bool {
	return ce.showSQL
}
