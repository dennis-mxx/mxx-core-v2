package mexception

import (
	"fmt"
	"reflect"
)

var (
	ERROR                     = Exception{1, "ERROR", ""}                     // 系统异常
	RunTimeException          = Exception{2, "RunTimeException", ""}          // 运行时异常
	NotFoundException         = Exception{3, "NotFoundException", ""}         // 不存在异常
	UnsupportedException      = Exception{4, "UnsupportedException", ""}      // 不支持
	IllegalParameterException = Exception{5, "IllegalParameterException", ""} // 非法参数
	EmptyException            = Exception{6, "EmptyException", ""}            // 空集合异常
	NilException              = Exception{7, "NilException", ""}              // 控制正异常
	DateTimeFormatException   = Exception{8, "DateTimeFormatException", ""}   // 日期格式化异常
	JsonParseException        = Exception{9, "JsonParseException", ""}        // JSON解析异常
	ParamsParseException      = Exception{10, "ParamsParseException", ""}     // 参数解析异常
	ValidationException       = Exception{11, "ValidationException", ""}      // 参数校验异常
	FileToMaxException        = Exception{12, "FileToMaxException", ""}       // 文件过大

	SQLException    = Exception{101, "SQLException", ""}    // sql 执行异常
	MongoException  = Exception{102, "MongoException", ""}  // mongodb 执行异常
	RedisException  = Exception{103, "RedisException", ""}  // redis 执行异常
	RabbitException = Exception{104, "RabbitException", ""} // rabbit 执行异常
)

type Exception struct {
	Code    int
	Name    string
	Message string
}

func (ce Exception) Msg(msg string) error {
	return Exception{Code: ce.Code, Name: ce.Name, Message: msg}
}

func (ce Exception) Error() string {
	return fmt.Sprintf("%s[%v]: 【%s】", ce.Name, ce.Code, ce.Message)
}

func NewException(ex Exception, message string) error {
	return Exception{ex.Code, ex.Name, message}
}
func NewError(err error) error {
	return Exception{ERROR.Code, ERROR.Name, err.Error()}
}
func NewErrorEx(ex Exception, err error) error {
	return Exception{ex.Code, ex.Name, err.Error()}
}

func IsMException(err any) bool {
	return reflect.TypeOf(err).String() == "mexception.Exception"
}
func Message(err error) string {
	if IsMException(err) {
		return err.(Exception).Message
	} else {
		return err.Error()
	}
}
