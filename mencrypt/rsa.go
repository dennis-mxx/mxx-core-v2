package mencrypt

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
)

var RSA = &rsaUtil{}

type rsaUtil struct {
}

func (domain rsaUtil) GeneratePkcs8Key(transcoder Transcoder, len int) (privateKey string, publicKey string, err error) {
	private, error := rsa.GenerateKey(rand.Reader, len)
	if error != nil {
		return "", "", error
	}
	privateByte, _ := x509.MarshalPKCS8PrivateKey(private)
	publicByte, _ := x509.MarshalPKIXPublicKey(&private.PublicKey)

	privateStr := string(transcoder.Encode(privateByte))
	publicStr := string(transcoder.Encode(publicByte))
	return privateStr, publicStr, nil
}
func (domain rsaUtil) GeneratePkcs1Key(transcoder Transcoder, len int) (privateKey string, publicKey string, err error) {
	private, error := rsa.GenerateKey(rand.Reader, len)
	if error != nil {
		return "", "", error
	}
	privateByte := x509.MarshalPKCS1PrivateKey(private)
	publicByte := x509.MarshalPKCS1PublicKey(&private.PublicKey)

	privateStr := string(transcoder.Encode(privateByte))
	publicStr := string(transcoder.Encode(publicByte))
	return privateStr, publicStr, nil
}
