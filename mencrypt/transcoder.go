package mencrypt

import (
	"encoding/base64"
	"encoding/hex"
)

var Hex hexTranscoder
var Base64 base64Transcoder

type Transcoder interface {
	Encode(src []byte) []byte
	Decode(src []byte) ([]byte, error)
}
type hexTranscoder struct {
}

func (domain hexTranscoder) Encode(src []byte) []byte {
	dst := make([]byte, hex.EncodedLen(len(src)))
	hex.Encode(dst, src) // 转码
	return dst
}

func (domain hexTranscoder) Decode(src []byte) ([]byte, error) {
	dst := make([]byte, hex.EncodedLen(len(src)))
	n, err := hex.Decode(dst, src) // 转码
	return dst[:n], err
}

type base64Transcoder struct {
}

func (domain base64Transcoder) Encode(src []byte) []byte {
	dst := make([]byte, base64.StdEncoding.EncodedLen(len(src)))
	base64.StdEncoding.Encode(dst, src)
	return dst
}
func (domain base64Transcoder) Decode(src []byte) ([]byte, error) {
	dst := make([]byte, base64.StdEncoding.EncodedLen(len(src)))
	n, err := base64.StdEncoding.Decode(dst, src)
	return dst[:n], err
}
