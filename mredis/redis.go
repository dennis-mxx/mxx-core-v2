package mredis

import (
	"fmt"
	"gitee.com/dennis-mxx/mxx-core-v2/menv"
	"gitee.com/dennis-mxx/mxx-core-v2/mexception"
	"gitee.com/dennis-mxx/mxx-core-v2/mlogger"
	"github.com/go-redis/redis"
	"time"
)

var RedisClient *redis.Client

func InitRedis(redisOptions *menv.Redis) {

	if redisOptions.Network == "" {
		redisOptions.Network = "tcp"
	}

	RedisClient = redis.NewClient(&redis.Options{
		Network:     redisOptions.Network,
		Addr:        redisOptions.Addr,
		Password:    redisOptions.Password,
		DB:          redisOptions.Db,
		ReadTimeout: time.Duration(redisOptions.ReadTimeout) * time.Second,
		DialTimeout: time.Duration(redisOptions.DialTimeout) * time.Second,
	})
	if RedisClient == nil {
		panic(mexception.NewException(mexception.RedisException, fmt.Sprintf("connection to redis session exception %s:%s [%v]", redisOptions.Network, redisOptions.Addr, redisOptions.Db)))
	} else {
		mlogger.Out.Info(fmt.Sprintf("Initialize [connection to redis session {%s:%s [%v]} successful]", redisOptions.Network, redisOptions.Addr, redisOptions.Db))
	}

}
