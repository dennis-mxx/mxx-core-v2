package mlogger

import (
	"bytes"
	"fmt"
	"gitee.com/dennis-mxx/mxx-core-v2/menv"
	"gitee.com/dennis-mxx/mxx-core-v2/mexception"
	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io"
	"os"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"time"
)

var Out *zap.SugaredLogger

var Adapter zapcore.CallerEncoder

const (
	outputDir = "./logs/"
)

func InitZap() {
	var level zapcore.Level
	switch menv.Environment.Logger.Level {
	case "debug":
	case "DEBUG":
		level = zapcore.DebugLevel
		break
	case "info":
	case "INFO":
		level = zapcore.InfoLevel
		break
	default:
		level = zapcore.InfoLevel
	}
	encoder := zapcore.NewConsoleEncoder(zapcore.EncoderConfig{
		MessageKey:    "msg",
		LevelKey:      "level",
		TimeKey:       "time",
		CallerKey:     "file",
		NameKey:       "logger",
		StacktraceKey: "stacktrace",
		LineEnding:    zapcore.DefaultLineEnding,
		EncodeLevel:   zapcore.CapitalLevelEncoder,
		EncodeCaller:  ShortCallerEncoder,
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05"))
		},
		//EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeDuration: func(d time.Duration, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendInt64(int64(d) / 1000000)
		},
	})

	logLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= level
	})

	errorLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl == zapcore.ErrorLevel
	})

	majorHook := getWriter(menv.Environment.ServerName + ".log")
	errorHook := getWriter(menv.Environment.ServerName + "-error.log")
	// 最后创建具体的Logger
	core := zapcore.NewTee(
		zapcore.NewCore(encoder, zapcore.AddSync(os.Stdout), logLevel),
		zapcore.NewCore(encoder, zapcore.AddSync(majorHook), logLevel),
		zapcore.NewCore(encoder, zapcore.AddSync(errorHook), errorLevel),
	)

	// 需要传入 zap.AddCaller() 才会显示打日志点的文件名和行数, 有点小坑
	log := zap.New(core, zap.AddCaller(), zap.AddStacktrace(zap.ErrorLevel))
	Out = log.Sugar()
	defer log.Sync()
	Out.Info(fmt.Sprintf("Initialize [logger level %s] ", level))
}
func ShortCallerEncoder(caller zapcore.EntryCaller, enc zapcore.PrimitiveArrayEncoder) {

	// TODO: consider using a byte-oriented API to save an allocation.
	enc.AppendString(caller.TrimmedPath())
	enc.AppendString(fmt.Sprintf("[%v]", gid()))
	if Adapter != nil {
		Adapter(caller, enc)
	}
}

func gid() uint64 {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, false)]
	b = bytes.TrimPrefix(b, []byte("goroutine "))
	b = b[:bytes.IndexByte(b, ' ')]
	n, _ := strconv.ParseUint(string(b), 10, 64)
	return n
}

func GetWriter() io.Writer {
	return getWriter(menv.Environment.ServerName + ".log")
}
func getWriter(filename string) io.Writer {
	dir := outputDir
	if menv.Environment.Logger != nil && menv.Environment.Logger.Dir != "" {
		dir = menv.Environment.Logger.Dir
	}

	if !strings.HasSuffix(dir, "/") {
		dir = dir + "/"
	}
	hook := zapcore.AddSync(&lumberjack.Logger{
		Filename:   dir + filename, //日志文件存放目录
		MaxSize:    15,             //文件大小限制,单位MB
		MaxBackups: 5,              //最大保留日志文件数量
		MaxAge:     15,             //日志文件保留天数
		Compress:   true,           //是否压缩处理
	})

	return hook
}

func FormatException() {
	defer func() {
		if err := recover(); err != nil {
			Out.Error(err)
		}
	}()
	if err := recover(); err != nil {
		FormatError(err)
	}

}
func FormatError(error any) {
	if error == nil {

	}
	stack := debug.Stack()
	var newStack []byte

	for newStackIndex, count, index := 0, 0, 0; index <= len(stack)/2; index++ {
		current := stack[index]
		if count <= 6 && current == 10 {
			count++
			continue
		}
		if count > 6 {
			if newStack == nil {
				newStack = make([]byte, len(stack)/2-index+1)
			}
			newStack[newStackIndex] = current
			newStackIndex++

			if current == 10 {
				newStack[newStackIndex] = 9
				newStackIndex++
				index++
			}
		}
	}

	message := strings.Trim(string(newStack), " ")
	var pr string
	if mexception.IsMException(error) {
		pr = fmt.Sprintf("\n\t%s\n\t%s0x1B", error, message)
	} else {
		pr = fmt.Sprintf("\n\tExceltion:\t【%s】\n\t%s0x1B", error, message)
	}
	Out.Error(pr)

}
