package mweb

import (
	"gitee.com/dennis-mxx/mxx-core-v2/menv"
	"github.com/gin-gonic/gin"
	"net/http/pprof"
)

func InitProf(engine *GinEngine) {

	engine.GET("/debug/pprof/", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Index(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})

	engine.GET("/debug/pprof/heap", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Index(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})
	engine.GET("/debug/pprof/heap?debug=1", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Index(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})
	engine.GET("/debug/pprof/mutex", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Index(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})
	engine.GET("/debug/pprof/block", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Index(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})
	engine.GET("/debug/pprof/goroutine", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Index(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})
	engine.GET("/debug/pprof/threadcreate", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Index(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})
	engine.GET("/debug/pprof/<script>scripty<script>", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Index(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})
	engine.GET("/debug/pprof/cmdline", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Cmdline(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})

	engine.GET("/debug/pprof/profile", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Profile(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})

	engine.GET("/debug/pprof/symbol", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Symbol(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})

	engine.GET("/debug/pprof/trace", func(context *gin.Context) {
		secret := context.Query("secret")
		if secret == menv.Environment.Gin.PprofSecret {
			pprof.Trace(context.Writer, context.Request)
		} else {
			context.AbortWithStatus(503)
		}
	})

}
