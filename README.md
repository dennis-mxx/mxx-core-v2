# MXX-CORE-V2

`一个帮助快速开发WEB项目的简易脚手架`

作者QQ：`2374227950`

## 文档更新日志

| 时间         | 更新内容  |
|------------|-------|
| 2023-03-31 | 项目初始化 |

## 环境介绍

内置以下模块，模块依靠配置进行加载，没有相关配置则不会运行

工具模块 mmodel,mutil () ,mtask

| 模块名称    | 引用           |
|---------|--------------|
| mysql   | mdb.Mysql    |
| redis   | mredis.Redis |
| sqlite3 | mdb.SqlIte3  |
| gin     | mweb.Engine  |

| 环境参数 | 配置文件地址                  |
|------|-------------------------|
| dev  | ./conf/config-dev.toml  |
| fat  | ./conf/config-fat.toml  |
| test | ./conf/config-test.toml |
| pro  | ./conf/config-pro.toml  |

执行模块加载代码 minit 下的init.go

```go
    minit.InitializeCmd("dev", nil) //第一个参数为默认环境，第二个参数为其余配置类若无则填nil

// 执行该行代码在生产环境启动时，可通过-env参数指定配置文件 环境参数 [pro,dev,test，fat]

```

## 配置详解

配置文件加载

`github.com/pelletier/go-toml/v2 v2.0.7`

### 非必须参数

| 配置名称       | 类型     | 默认值               | 描述    |
|------------|--------|-------------------|-------|
| serverName | string | KNOW SERVER NAME  | 服务名称  |
| banner     | string | minit下的banner.txt | 开屏页地址 |

### Logger  默认加载

`go.uber.org/zap v1.24.0`

| 配置名称  | 类型     | 默认值  | 描述   |
|-------|--------|------|------|
| level | string | info | 日志级别 |

### Mysql

Database 不为空时加载

`github.com/go-sql-driver/mysql v1.7.0`

`github.com/jmoiron/sqlx v1.3.5`

| 配置名称        | 类型     | 默认值   | 描述           |
|-------------|--------|-------|--------------|
| username    | string |       | 用户名          |
| password    | string |       | 密码           |
| addr        | string |       | 地址           |
| database    | string |       | 数据库名         |
| showSql     | bool   | false | 打印sql        |
| maxIdleConn | int    |       | 最大闲置链接数      |
| maxOpenConn | int    |       | 最大活跃连接数      |
| timeout     | int64  |       | 请求超时时间（单位毫秒） |

### Sqlite3

Addr不为空时加载

`github.com/mattn/go-sqlite3 v2.0.3+incompatible`

| 配置名称        | 类型     | 默认值   | 描述           |
|-------------|--------|-------|--------------|
| addr        | string |       | 数据源地址        |
| showSql     | bool   | false | 打印sql        |
| maxIdleConn | int    |       | 最大闲置链接数      |
| maxOpenConn | int    |       | 最大活跃连接数      |
| timeout     | int64  |       | 请求超时时间（单位毫秒） |

### Gin

Port不为空时加载

`github.com/gin-gonic/gin v1.9.0`

`github.com/gorilla/securecookie v1.1.1`

`github.com/gorilla/sessions v1.2.1`

| 配置名称          | 类型     | 默认值                              | 描述                                   |
|---------------|--------|----------------------------------|--------------------------------------|
| port          | int64  |                                  | 端口号                                  |
| basePath      | string |                                  | 请求根路径                                |
| sessions      | string | cookie                           | session类型   cookie,file, redis       |
| sessionId     | string | sessionId                        | sessionId名称                          |
| sessionFile   | string |                                  | session文件位置   (当session存储类型为file时必填) |
| sessionSecret | string | v0XqbECSxbhMVocsJLaYPGabE7236Agu | session密钥                            |

### Redis

Addr不为空时加载

`github.com/go-redis/redis v6.15.9+incompatible`

| 配置名称        | 类型     | 默认值   | 描述               |
|-------------|--------|-------|------------------|
| network     | string |       | 链接类型，无特殊处理则为 tcp |
| addr        | string | false | 链接地址 host:port   |
| password    | string |       | 密码               |
| db          | int    | 0     | 数据库              |
| dialTimeout | int    |       | 请求超时（单位毫秒）       |
| readTimeout | int    |       | 读取超时（单位毫秒）       |

# 示例代码

## 启动示例

使用前可详细阅读工具模块 mutil , mmodel

```go

import (
"fmt"
"gitee.com/dennis-mxx/mxx-core-v2/minit"
"gitee.com/dennis-mxx/mxx-core-v2/mmodel"
"gitee.com/dennis-mxx/mxx-core-v2/mweb"
"github.com/gin-gonic/gin"
)

func main() {
minit.InitializeCmd("dev", nil)

mweb.Engine.UseMiddleware()
mweb.Engine.WithCors()
mweb.Engine.XXGet("/", func (stx mweb.SessionCtx, ctx *gin.Context) (result any, err error) {
session, _ := stx.GetSession()
if session.ID == "" {
session.Values["id"] = "1"
stx.Save()
}
fmt.Println(session.Values)
return mmodel.Ok(), nil
})
mweb.Engine.XXRun()
}


```

## 数据库操作示例

```go

import (
"gitee.com/dennis-mxx/mxx-core-v2/mdb"
"gitee.com/dennis-mxx/mxx-core-v2/mmodel"
)

type EntityWxConfig struct {
Id                  int64                 `db:"id"`
AppName             mmodel.String         `db:"app_name"` // mmodel.String 可接受所有数据库字段类型包括 time
AppId               string                `db:"app_id"`
Secret              string                `db:"secret"`
AccessToken         string                `db:"access_token"`
AccessTokenExpires  int64                 `db:"access_token_expires"`
Locked              int64                 `db:"locked"`
AccessTokenCreateAt *mmodel.LocalDatetime `db:"access_token_create_at"`
}

func (domain *EntityWxConfig) TableName() string {
return "tb_wx_config"
}

func (domain *EntityWxConfig) SelectByAppId() bool {
query := mdb.NewSelect(domain.TableName()).SelectAll().Equal(map[string]any{
"app_id": domain.AppId,
})
mdb.MySql.SelectRow(domain, query.SelectSql(), query.SelectParam()...)
return domain.Id != 0
}

func (domain *EntityWxConfig) GetLockedFailure() bool {
return mdb.MySql.Update("update wx_config set locked = 1 where app_id = ? and locked = 0 ", domain.AppId) == 0
}

func (domain *EntityWxConfig) UnLocked() bool {
mdb.NewUpdate(domain.TableName()).Set(map[string]any{
"locked": 0,
"app_id": domain.AppId,
})
return mdb.MySql.UpdateMap(domain.TableName(), map[string]any{
"locked": 0,
"app_id": domain.AppId,
}, map[string]any{
"app_id": domain.AppId,
}) > 0
}

func (domain *EntityWxConfig) Insert() {
sql, param := mdb.NewInsert(domain.TableName()).Insert(map[string]any{
"app_id":                 domain.AppId,
"app_name":               domain.AppName,
"secret":                 domain.Secret,
"access_token_create_at": domain.AccessTokenCreateAt,
"access_token_expires":   0,
"locked":                 0,
"access_token":           "",
})
mdb.MySql.Insert(sql, param...)

}

```