package mutil

import (
	"gitee.com/dennis-mxx/mxx-core-v2/mexception"
)

func Watch[T any](t T, err error) T {
	if err != nil {
		panic(err)
	}
	return t
}
func Watch0[T any](err error) {
	if err != nil {
		panic(err)
	}
}
func Watch2[T any, E any](t T, e E, err error) (T, E) {
	if err != nil {
		panic(err)
	}
	return t, e
}

func Watch3[T any, E any, F any](t T, e E, f F, err error) (T, E, F) {
	if err != nil {
		panic(err)
	}
	return t, e, f
}

func Watch4[T any, E any, F any, G any](t T, e E, f F, g G, err error) (T, E, F, G) {
	if err != nil {
		panic(err)
	}
	return t, e, f, g
}

func Watch5[T any, E any, F any, G any, H any](t T, e E, f F, g G, h H, err error) (T, E, F, G, H) {
	if err != nil {
		panic(err)
	}
	return t, e, f, g, h
}
func Watch6[T any, E any, F any, G any, H any, I any](t T, e E, f F, g G, h H, i I, err error) (T, E, F, G, H, I) {
	if err != nil {
		panic(err)
	}
	return t, e, f, g, h, i
}

func Assert(msg string, err error) {
	if err != nil {
		panic(mexception.ERROR.Msg(msg + " >>> " + err.Error()))
	}
}
