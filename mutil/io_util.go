package mutil

import (
	"bytes"
	"io"
)

func ReadBuffer(reader io.Reader) (*bytes.Buffer, error) {
	buffer := &bytes.Buffer{}
	_, err := buffer.ReadFrom(reader)
	return buffer, err
}
