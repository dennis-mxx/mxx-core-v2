package mutil

import (
	"encoding/json"
	"gitee.com/dennis-mxx/mxx-core-v2/mexception"
	"reflect"
)

func JSONTOString(data any) (jsonStr string, err error) {
	jsonByte, error := json.Marshal(data)
	if error != nil {
		return "", mexception.NewErrorEx(mexception.JsonParseException, error)
	}
	return string(jsonByte), nil
}
func JSONClone(input any, output any) error {
	var errs error
	if reflect.TypeOf(input).Kind() == reflect.String {
		return json.Unmarshal([]byte(input.(string)), output)
	} else {
		bytes, err := json.Marshal(input)
		if err != nil {
			return mexception.NewErrorEx(mexception.JsonParseException, err)
		}

		if errs = json.Unmarshal(bytes, output); errs != nil {
			return mexception.NewErrorEx(mexception.JsonParseException, errs)
		}
	}
	return nil

}
