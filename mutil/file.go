package mutil

import (
	"bufio"
	"os"
	"strings"
)

func ReadFileLines(inputFilePath string) ([]string, error) {
	if b, err := os.ReadFile(inputFilePath); err == nil {
		return strings.Split(string(b), "\r\n"), err

	} else {
		return nil, err
	}
}

func ReadFileLines2(inputFilePath string, spit string) ([][]string, error) {
	if arr, err := ReadFileLines(inputFilePath); err == nil {
		var itemList [][]string
		for _, item := range arr {
			itemList = append(itemList, strings.Split(item, spit))
		}
		return itemList, nil
	} else {
		return nil, err
	}
}
func WriteFileLine(outputFilePath string, lines []string) error {
	return os.WriteFile(outputFilePath, []byte(strings.Join(lines, "\r\n")), 0666)
}

func ReadBigFile(inputFilePath string, fn func(line string)) error {
	inputFile, err := os.Open(inputFilePath)
	if err != nil {
		return err
	}
	scanner := bufio.NewScanner(inputFile)
	for scanner.Scan() {
		line := scanner.Text()
		fn(line)
	}
	if err := scanner.Err(); err != nil {
		return err
	} else {
		return nil
	}
}
