package mutil

import (
	"fmt"
	"gitee.com/dennis-mxx/mxx-core-v2/mexception"
	"math/rand"
	"strings"
	"time"
)

var RandomUtil = &randomUtil{
	LowerCaseAZ: []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"},
	UpCaseAZ:    []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"},
	Number:      []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"},
	All: []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
		"V", "W", "X", "Y", "Z"},
}

type randomUtil struct {
	LowerCaseAZ []string
	UpCaseAZ    []string
	Number      []string
	All         []string
}

func init() {
	rand.Seed(time.Now().Unix())

}

func (domain randomUtil) Next(max int) int {
	return rand.Intn(max)
}
func (domain randomUtil) NextXaz(len int) string {
	result := strings.Builder{}
	for i := 0; i < len; i++ {
		result.WriteString(domain.LowerCaseAZ[domain.Next(26)])
	}
	return result.String()
}
func (domain randomUtil) NextXAZ(len int) string {
	result := strings.Builder{}
	for i := 0; i < len; i++ {
		result.WriteString(domain.UpCaseAZ[domain.Next(26)])
	}
	return result.String()
}
func (domain randomUtil) NextXaZ09(len int) string {
	result := strings.Builder{}
	for i := 0; i < len; i++ {
		result.WriteString(domain.All[domain.Next(61)])
	}
	return result.String()
}
func (domain randomUtil) UniqueId(prefix string, len int) string {
	if len < 16 {
		panic(mexception.NewException(mexception.IllegalParameterException, fmt.Sprintf("len = %v ,min len >= 16", len)))
	}

	result := strings.Builder{}
	result.WriteString(prefix)
	result.WriteString(DateUtil.Format(DateUtil.DateTimeMillNumberFormat))
	for i := 0; i < len-12; i++ {
		result.WriteString(domain.All[domain.Next(61)])
	}
	return result.String()
}
