package mutil

import (
	"gitee.com/dennis-mxx/mxx-core-v2/mexception"
	"gitee.com/dennis-mxx/mxx-core-v2/mlogger"
	"github.com/howcrazy/xconv"
	"reflect"
	"strconv"
)

func init() {

	defer mlogger.FormatException()

	BoolTypes := []any{reflect.Bool}

	convertMap := &xconv.ConvertMap
	// int 转 string
	(*convertMap).Set(xconv.IntTypes, xconv.StringTypes, func(convertor *xconv.Convertor, src reflect.Value, dst reflect.Value) {
		dst.SetString(strconv.Itoa(int(src.Int())))
	})
	// float 转string
	(*convertMap).Set(xconv.FloatTypes, xconv.StringTypes, func(convertor *xconv.Convertor, src reflect.Value, dst reflect.Value) {
		dst.SetString(strconv.FormatFloat(src.Float(), 'g', -1, 32))
	})
	// bool 转string
	(*convertMap).Set(BoolTypes, xconv.StringTypes, func(convertor *xconv.Convertor, src reflect.Value, dst reflect.Value) {
		dst.SetString(strconv.FormatBool(src.Bool()))
	})

	// string 转 int
	(*convertMap).Set(xconv.StringTypes, xconv.IntTypes, func(convertor *xconv.Convertor, src reflect.Value, dst reflect.Value) {
		i, error := strconv.ParseInt(src.String(), 10, 64)
		if error != nil {
			panic(mexception.NewError(error))
		}
		dst.SetInt(i)
	})
	// string 转 float
	(*convertMap).Set(xconv.StringTypes, xconv.FloatTypes, func(convertor *xconv.Convertor, src reflect.Value, dst reflect.Value) {
		f, error := strconv.ParseFloat(src.String(), 64)
		if error != nil {
			panic(mexception.NewError(error))
		}
		dst.SetFloat(f)
	})

	// string 转 bool
	(*convertMap).Set(xconv.StringTypes, BoolTypes, func(convertor *xconv.Convertor, src reflect.Value, dst reflect.Value) {
		b, error := strconv.ParseBool(src.String())
		if error != nil {
			panic(mexception.NewError(error))
		}
		dst.SetBool(b)
	})
}
