package mutil

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"hash/fnv"
)

func Hash(data string) (uint32, error) {
	h := fnv.New32a()
	if _, err := h.Write([]byte(data)); err == nil {
		return h.Sum32(), nil
	} else {
		return 0, err
	}
}

func Md5(data string) string {
	hasher := md5.New()
	hasher.Write([]byte(data))
	hashBytes := hasher.Sum(nil)
	return hex.EncodeToString(hashBytes)
}

func Sha256Hash(data string) string {
	hasher := sha256.New()
	hasher.Write([]byte(data))
	hashBytes := hasher.Sum(nil)
	return hex.EncodeToString(hashBytes)
}
