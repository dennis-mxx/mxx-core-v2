package mmodel

type CmdPage struct {
	Page   String   `json:"page" form:"page" uri:"page" binding:"required"`
	Size   String   `json:"size" form:"size" uri:"size" binding:"required"`
	Sorter []string `json:"sorter" form:"sorter" uri:"sorter"`
}

func (ew CmdPage) GetPage() int64 {
	if ew.Page.IsEmpty() {
		return 1
	}
	return ew.Page.Int64()
}
func (ew CmdPage) GetSize() int64 {
	if ew.Size.IsEmpty() {
		return 10
	}
	return ew.Size.Int64()
}

type Resp struct {
	Code int `json:"code"`
	Data any `json:"data"`
	Err  Err `json:"err"`
}
type Err struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
}

func Failure(code int, errCode string, message string) *Resp {
	return &Resp{
		Code: code,
		Err: Err{
			Code: errCode,
			Msg:  message,
		},
	}
}

func Fail(message string) *Resp {
	return &Resp{
		Code: 1,
		Err: Err{
			Code: "fail",
			Msg:  message,
		},
	}
}

func Ok() *Resp {
	return &Resp{
		Code: 0,
	}
}

func OkD(data any) *Resp {
	return &Resp{
		Code: 0,
		Data: data,
	}
}

func (domain *Resp) Ok() *Resp {
	domain.Code = 0
	return domain
}
