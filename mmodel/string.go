package mmodel

import (
	"database/sql/driver"
	"fmt"
	"gitee.com/dennis-mxx/mxx-core-v2/mutil"
	"strings"
	"time"
)

type String string

func (domain *String) String() string {
	return string(*domain)
}

func (domain *String) Value() (driver.Value, error) {
	return domain.String(), nil
}

func (domain *String) Scan(src any) error {
	switch src.(type) {
	case []uint8:
		*domain = String(string(src.([]uint8)))
		return nil
	case time.Time:
		*domain = String((src.(time.Time)).String())
		return nil
	default:
		*domain = String(fmt.Sprintf("%v", src))
		return nil
	}
}

func (domain *String) Int64() int64 {
	s := domain.String()
	if i := strings.Index(s, "."); i != -1 {
		return mutil.Strings.ToInt64(s[0:i])
	}
	return mutil.Strings.ToInt64(domain)
}

func (domain *String) Int8() int8 {
	s := domain.String()
	if i := strings.Index(s, "."); i != -1 {
		return mutil.Strings.ToInt8(s[0:i])
	}
	return mutil.Strings.ToInt8(domain)
}
func (domain *String) Int() int {
	s := domain.String()
	if i := strings.Index(s, "."); i != -1 {
		return mutil.Strings.ToInt(s[0:i])
	}
	return mutil.Strings.ToInt(domain)
}
func (domain *String) Int32() int32 {
	s := domain.String()
	if i := strings.Index(s, "."); i != -1 {
		return mutil.Strings.ToInt32(s[0:i])
	}
	return mutil.Strings.ToInt32(domain)
}

func (domain *String) Int16() int16 {
	s := domain.String()
	if i := strings.Index(s, "."); i != -1 {
		return mutil.Strings.ToInt16(s[0:i])
	}
	return mutil.Strings.ToInt16(domain)
}

func (domain *String) Bool() bool {
	return mutil.Strings.ToBool(domain)
}

func (domain *String) Float32() float32 {
	return mutil.Strings.ToFloat32(domain)
}

func (domain *String) Float64() float64 {
	return mutil.Strings.ToFloat64(domain)
}

func (domain *String) IsEmpty() bool {
	return domain == nil || len(*domain) == 0
}

func (domain *String) Length() int {
	return len(*domain)
}

func (domain *String) EqualFold(val string) bool {
	return strings.EqualFold(domain.String(), val)
}

func (domain *String) HasSuffix(suffix string) bool {
	return strings.HasSuffix(domain.String(), suffix)
}

func (domain *String) HasPrefix(prefix string) bool {
	return strings.HasPrefix(domain.String(), prefix)
}

func (domain *String) Index(substr string) int {
	return strings.Index(domain.String(), substr)
}

func (domain *String) IndexAny(chars string) int {
	return strings.IndexAny(domain.String(), chars)
}

func (domain *String) LastIndex(substr string) int {
	return strings.LastIndex(domain.String(), substr)
}

func (domain *String) LastIndexAny(chars string) int {
	return strings.LastIndexAny(domain.String(), chars)
}
func (domain *String) Time() time.Time {
	length := len(*domain)
	if length == 8 {
		return mutil.DateUtil.StrToTime(domain.String())
	} else if length == 10 {
		return mutil.DateUtil.StrToDate(domain.String())
	} else if length == 19 || length == 27 {
		return mutil.DateUtil.StrToDateTime(domain.String())
	} else if length == 29 || length == 37 {
		date := (*domain)[0:19]
		return mutil.DateUtil.StrToDateTime(date.String())
	}
	panic(fmt.Sprintf("%s is not a valid date format !", domain.String()))
}
func (domain *String) LocalDateTime() LocalDatetime {
	return LocalDatetime(domain.Time())
}
func (domain *String) UnmarshalJSON(data []byte) (err error) {
	*domain = ReadString(data)
	return
}
func ReadString(data []byte) String {
	length := len(data)
	if length >= 2 {
		if data[0] == 34 && data[length-1] == 34 {
			data = data[1 : length-1]
		}
	}
	return String(data)
}
