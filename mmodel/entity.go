package mmodel

type Ext struct {
	CreateTime     *LocalDatetime `xorm:"datetime comment('创建时间')" json:"createTime"`
	CreateUserCode string         `xorm:"varchar(60) comment('创建人') " json:"createUserCode"`
	CreateUserName string         `xorm:"varchar(100) comment('创建人名称') " json:"createUserName"`
	UpdateTime     *LocalDatetime `xorm:"datetime comment('修改时间') " json:"updateTime"`
	UpdateUserCode string         `xorm:"varchar(60) comment('修改人') " json:"updateUserCode"`
	UpdateUserName string         `xorm:"varchar(100) comment('修改人名称') " json:"updateUserName"`
	Version        int64          `xorm:"version bigint(200) comment('版本号') " json:"version"`
	Deleted        *LocalDatetime `xorm:"deleted datetime comment('删除时间') " json:"deleted"`
}

func ExtInsertCols(cols ...string) []string {
	res := []string{
		"create_time",
		"create_user_code",
		"create_user_name",
	}
	res = append(res, ExtUpdateCols(cols...)...)
	return res
}
func ExtUpdateCols(cols ...string) []string {
	res := []string{
		"update_time",
		"update_user_code",
		"update_user_name",
		"version",
	}
	res = append(res, cols...)
	return res
}

func (ew *Ext) C(user CodeName) Ext {
	now := NowLocalDateTime()

	ew.CreateUserCode = user.CCode()
	ew.CreateUserName = user.CName()
	ew.CreateTime = now

	ew.UpdateUserCode = user.CCode()
	ew.UpdateUserName = user.CName()
	ew.UpdateTime = now
	return *ew
}

func (ew *Ext) M(user CodeName) Ext {
	now := NowLocalDateTime()
	ew.UpdateUserCode = user.CCode()
	ew.UpdateUserName = user.CName()
	ew.UpdateTime = now
	return *ew
}

type CodeName interface {
	CName() string
	CCode() string
}

type SPUser struct {
	UserCode string `json:"userCode"`
	UserName string `json:"userName"`
}

func (ew SPUser) CName() string {
	return ew.UserName
}
func (ew SPUser) CCode() string {
	return ew.UserCode
}
