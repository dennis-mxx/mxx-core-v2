package mmodel

type Pageable struct {
	Content        any   `json:"content"`
	Page           int64 `json:"page"`
	Size           int64 `json:"size"`
	First          bool  `json:"first"`
	Last           bool  `json:"last"`
	MaxPage        int64 `json:"lastPage"`
	TotalElements  int64 `json:"totalElements"`
	CurrentElement int64 `json:"currentElement"`
}

func (domain *Pageable) IsEmpty() bool {
	return domain.CurrentElement == 0
}

func (domain *Pageable) NotEmpty() bool {
	return domain.CurrentElement > 0
}
