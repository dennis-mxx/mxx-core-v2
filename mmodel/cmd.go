package mmodel

type CmdID struct {
	ID int64 `json:"id" form:"id" uri:"id"  binding:"required"`
}

type CmdIDStr struct {
	ID string `json:"id"  form:"id" uri:"id"  binding:"required"`
}

type CmdStat struct {
	ID      string `json:"id"  binding:"required"`
	NewStat bool   `json:"newStat" `
	OldStat bool   `json:"oldStat" `
}
type CmdCode struct {
	Code string `json:"code"  form:"code" uri:"code" binding:"required"`
}
