package mbolt

func GetBytes(bucketName string, key string) ([]byte, error) {
	if tran, err := getDefault().R(); err == nil {
		defer tran.Rollback()
		return tran.Bucket(bucketName).GetBytes(key), nil
	} else {
		return nil, err
	}
}

func Get(bucketName string, key string) (string, error) {
	if tran, err := getDefault().R(); err == nil {
		defer tran.Rollback()
		return tran.Bucket(bucketName).Get(key), nil
	} else {
		return "", err
	}
}

func SetBytes(bucketName string, key string, value []byte) error {
	if tran, err := getDefault().R(); err == nil {
		defer tran.Commit()
		return tran.Bucket(bucketName).SetBytes(key, value)
	} else {
		return err
	}
}

func Set(bucketName string, key string, value string) error {
	if tran, err := getDefault().R(); err == nil {
		defer tran.Commit()
		return tran.Bucket(bucketName).Set(key, value)
	} else {
		return err
	}
}
func Delete(bucketName string, key string) error {
	if tran, err := getDefault().R(); err == nil {
		defer tran.Commit()
		return tran.Bucket(bucketName).Delete(key)
	} else {
		return err
	}
}
func GetJson(bucketName string, key string, dest any) (any, error) {
	if tran, err := getDefault().R(); err == nil {
		defer tran.Commit()
		return tran.Bucket(bucketName).GetJson(key, dest)
	} else {
		return nil, err
	}
}

func SetJson(bucketName string, key string, dest any) error {
	if tran, err := getDefault().R(); err == nil {
		defer tran.Commit()
		return tran.Bucket(bucketName).SetJson(key, dest)
	} else {
		return err
	}
}

func getDefault() *Bolt {
	return Default
}
